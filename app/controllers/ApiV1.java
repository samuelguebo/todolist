package controllers;
import play.libs.Json;
import org.codehaus.jackson.JsonNode;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;


import play.data.* ;
import models.*;
import play.*;
import play.mvc.*;

import views.html.*;


public class ApiV1 extends Controller {
	static Form<Task> taskForm = Form.form(Task.class);

	public static Result getTask(Long id) {
		Task task = Task.get(id);
		return ok(Json.toJson(task));
	}
	
    public static Result index() {
		return redirect(routes.ApiV1.tasks());

    }
	public static Result tasks() {
		return ok(Json.toJson(Task.all()));
	}
	public static Result addTask() {
	  Form<Task> filledForm = taskForm.bindFromRequest();
	  if(filledForm.hasErrors()) {
		return badRequest();
	  } else {
		Task.create(filledForm.get());
	  }
	  return ok();
	}


	public static Result deleteTask(Long id) {
		Task.delete(id);
		// return redirect(routes.Application.tasks());
		return ok();
	}


}
