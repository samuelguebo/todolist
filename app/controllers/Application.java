package controllers;
import play.libs.Json;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;


import play.data.* ;
import models.*;
import play.*;
import play.mvc.*;

import views.html.*;


public class Application extends Controller {
	static Form<Task> taskForm = Form.form(Task.class);

	public static Result viewJson(Long id) {
		// return TODO;
		Task task = Task.get(id);
		
		// ObjectNode result = Json.newObject();
		// result.put("label", task.label);
		// result.put("description", task.description);
		// return ok(Json.toJson(task));
		return ok(Json.toJson(task));
	}
	
    public static Result index() {
        // return ok(index.render("Your new application is ready."));
		//return ok("Hello World!");
		return redirect(routes.Application.tasks());

    }
	public static Result tasks() {
		// return TODO;
		return ok(
		// views.html.index.render(Task.all(), taskForm)
		views.html.index.render(Task.all())
		);
	}
	public static Result newTask() {
	  Form<Task> filledForm = taskForm.bindFromRequest();
	  if(filledForm.hasErrors()) {
		return badRequest(
		  views.html.add.render(filledForm)
		);
	  } else {
		Task.create(filledForm.get());
		return redirect(routes.Application.tasks());  
	  }
	}
	// public static Result taskForm() {
		// return ok(
				  // views.html.add.render()
				// );
	// }	
	/*public static Result editTask() {
	  Form<Task> filledForm = taskForm.bindFromRequest();
	  if(filledForm.hasErrors()) {
		return badRequest(
		  views.html.index.render(Task.all(), filledForm)
		);
	  } else {
		Task.create(filledForm.get());
		return redirect(routes.Application.tasks());  
	  }
	}*/

	public static Result viewTask(Long id) {
		// return TODO;
		return ok(
		views.html.single.render(Task.get(id))
		);
	}

	public static Result deleteTask(Long id) {
		Task.delete(id);
		return redirect(routes.Application.tasks());
	}


}
